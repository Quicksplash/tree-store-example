import { TreeStore } from './treeStore';
import { Item } from './types';

describe('TreeStore', () => {
  let items: Item[];
  let treeStore: TreeStore;

  beforeEach(() => {
    items = [
      { id: 1, parent: 'root' },
      { id: 2, parent: 1, type: 'test' },
      { id: 3, parent: 1, type: 'test' },
      { id: 4, parent: 2, type: 'test' },
      { id: 5, parent: 2, type: 'test' },
      { id: 6, parent: 2, type: 'test' },
      { id: 7, parent: 4, type: null },
      { id: 8, parent: 4, type: null },
    ];
    treeStore = new TreeStore(items);
  });

  test('getAll должен возврашать все элементы', () => {
    expect(treeStore.getAll()).toEqual(items);
  });

  test('getItem должен возвращать корректный элемент по идентификатору', () => {
    expect(treeStore.getItem(3)).toEqual(items[2]);
  });

  test('getChildren должен возвращать прямых потомков указанного элемента', () => {
    expect(treeStore.getChildren(2)).toEqual([items[3], items[4], items[5]]);
  });

  test('getAllChildren должен возвращать всех потомков указанного элемента', () => {
    expect(treeStore.getAllChildren(2)).toEqual([items[3], items[4], items[5], items[6], items[7]]);
  });

  test('getAllParents должен возвращать всех предков до корня', () => {
    expect(treeStore.getAllParents(4)).toEqual([items[0], items[1]]);
  });

  test('getChildren должен возвращать пустой массив, если потомков нет', () => {
    expect(treeStore.getChildren(5)).toEqual([]);
  });
});
