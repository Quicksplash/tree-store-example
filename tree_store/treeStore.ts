import type { Item, ItemID } from './types';

export class TreeStore {
  //Использую Map чтобы каждый раз не искать в массиве
  
  private itemsMap: Map<ItemID, Item>;
  private childrenMap: Map<ItemID, Item[]>;

  constructor(items: Item[]) {
      this.itemsMap = new Map();
      this.childrenMap = new Map();

      // Инициализация maps
      items.forEach(item => {
          this.itemsMap.set(item.id, item);

          // Инициализация children map
          if (!this.childrenMap.has(item.parent)) {
              this.childrenMap.set(item.parent, []);
          }
          this.childrenMap.get(item.parent)?.push(item);
      });
  }

  getAll(): Item[] {
      return Array.from(this.itemsMap.values());
  }

  getItem(id: ItemID): Item | undefined {
      return this.itemsMap.get(id);
  }

  getChildren(id: ItemID): Item[] {
      return this.childrenMap.get(id) || [];
  }

  getAllChildren(id: ItemID): Item[] {
      const result: Item[] = [];
      const collectChildren = (currentId: number | string) => {
          const children = this.getChildren(currentId);
          children.forEach(child => {
              result.push(child);
              collectChildren(child.id);
          });
      };
      collectChildren(id);
      return result;
  }

  getAllParents(id: ItemID): Item[] {
      const path: Item[] = [];
      let current = this.getItem(id);
      while (current && current.parent !== 'root') {
          current = this.getItem(current.parent);
          if (current) {
              path.push(current);
          }
      }
      return path.reverse();
  }
}