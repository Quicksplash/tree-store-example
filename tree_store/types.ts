export type ItemID = number | string;

export interface Item {
  id: ItemID;
  parent: number | string;
  type?: any;
};
